import Vue from 'vue'
import Router from 'vue-router'
import AmbientLightSensorExploit from '@/components/AmbientLightSensorExploit'
import BatteryStatus from '@/components/BatteryStatus'
import DeviceOrientation from '@/components/DeviceOrientation'
import IndexedDB from '@/components/IndexedDB'
import NavigatorOnline from '@/components/NavigatorOnline'
import PageVisibility from '@/components/PageVisibility'
import ShapeDetection from '@/components/ShapeDetection'
import SpeechRecognition from '@/components/SpeechRecognition'
import SpeechSynthesis from '@/components/SpeechSynthesis'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/AmbientLightSensorExploit',
      name: 'AmbientLightSensorExploit',
      component: AmbientLightSensorExploit
    },
    {
      path: '/BatteryStatus',
      name: 'BatteryStatus',
      component: BatteryStatus
    },
    {
      path: '/DeviceOrientation',
      name: 'DeviceOrientation',
      component: DeviceOrientation
    },
    {
      path: '/IndexedDB',
      name: 'IndexedDB',
      component: IndexedDB
    },
    {
      path: '/NavigatorOnline',
      name: 'NavigatorOnline',
      component: NavigatorOnline
    },
    {
      path: '/PageVisibility',
      name: 'PageVisibility',
      component: PageVisibility
    },
    {
      path: '/ShapeDetection',
      name: 'ShapeDetection',
      component: ShapeDetection
    },
    {
      path: '/SpeechRecognition',
      name: 'SpeechRecognition',
      component: SpeechRecognition
    },
    {
      path: '/SpeechSynthesis',
      name: 'SpeechSynthesis',
      component: SpeechSynthesis
    }
  ]
})
