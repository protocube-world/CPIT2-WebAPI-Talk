![](static/cover.jpg)

# CPIT2-WebAPI-Talk

> Campus Party Italia 2018 - WebAPI Talk material

## Requirements

In order to successfully run the application you need to provide a GIPHY API KEY. You can obtain your own key [by creating a new GIPHY App](https://developers.giphy.com/dashboard/?create=true) (you'll need a GIPHY account for this).

When you have obtained the key you must create the `src/GIPHY_API_KEY.json` file with the following content:

```json
{
  "GIPHY_API_KEY": "YOUR_BRAND_NEW_GIPHY_API_KEY"
}
```

**N.B.:** in order to be able to use the Shape Detection tools you must enable the following flag in chrome

```
chrome://flags/#enable-experimental-web-platform-features
```

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
